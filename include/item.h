#ifndef ITEM_H
#define ITEM_H

#include "hump-typedef.h"

#include <QString>
#include <QImage>

#include <functional>


class QPainter;

class Layout;
class ItemGroup;

class Item
{
public:
    ///
    explicit Item(Layout* _parentLay);

    ///
    explicit Item(ItemGroup* _group);

    ///
    void setSvg(QString _svgName);

    ///
    void move(h_ln _x, h_ln _y);

    ///
    void move(h_pt _pt);

    ///
    h_pt pos() const;

//    ///
//    void setDefaultSize(h_ln _w, h_ln _h);

//    ///
//    void setDefaultSize(h_sz _sz);

    ///
    void resize(h_ln _w, h_ln _h);

    ///
    void resize(h_sz _sz);

    ///
    h_sz size() const;

    ///
    void draw(QPainter& _p);

    ///
    void applyFactor();


private:
    ///
    Layout* lay_;

    ///
    h_pt pos_;

    ///
//    h_sz defaultSize_;

    ///
    h_sz size_;

    ///
    ItemGroup* gr_;

    ///
    QString svgName_;

    ///
    QImage rasterBuffer_;


    ///
    void prepareBuffer_();

};

#endif // ITEM_H
