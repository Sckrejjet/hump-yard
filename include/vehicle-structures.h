#ifndef VEHICLE_STRUCTURES_H
#define VEHICLE_STRUCTURES_H

#include "hump-typedef.h"

struct vehicle_init_data_t
{
    ///
    h_flt x;

    ///
    h_flt v;

    ///
    h_flt a;

    ///
    h_flt brX;

    ///
    h_flt brLen;

    ///
    h_flt step1;

    ///
    h_flt step2;

    ///
    h_flt step3;


    vehicle_init_data_t()
        : x(0.0f)
        , v(0.0f)
        , a(0.0f)
        , brX(0.0f)
        , brLen(0.0f)
        , step1(0.0f)
        , step2(0.0f)
        , step3(0.0f)
    {

    }

};

inline h_flt toMPS(h_flt _vel)
{
    return  _vel * 21.5f;
}

inline h_flt fromMPS(h_flt _vel)
{
    return  _vel / 21.5f;
}

#endif // VEHICLE_STRUCTURES_H
