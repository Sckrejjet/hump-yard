#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QWidget>


class Scene;
class PanelSettings;


class CentralWidget : public QWidget
{
    Q_OBJECT

public:
    ///
    explicit CentralWidget(QWidget* _parent = Q_NULLPTR);

    ///
    ~CentralWidget();


public slots:
    ///
    void slotInitDataChanged();


private slots:
    ///
    void resizeEvent(QResizeEvent* _e);


private:
    ///
    Scene* scene_;

    ///
    PanelSettings* panelSettings_;

};

#endif // CENTRALWIDGET_H
