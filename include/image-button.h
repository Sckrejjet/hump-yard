#ifndef IMAGE_BUTTON_H
#define IMAGE_BUTTON_H

#include <QPushButton>
#include <QImage>


class ImageButton : public QPushButton
{
    Q_OBJECT
    
public:
    ///
    explicit ImageButton(QWidget* _parent = Q_NULLPTR);
    
    ///
    ~ImageButton();

    ///
    void setSvg(QString _pressed, QString _released);


signals:
    ///
    void signalPressed();

    ///
    void signalReleased();


private slots:
    ///
    void resizeEvent(QResizeEvent* _e);

    ///
    void mousePressEvent(QMouseEvent* _e);

    ///
    void mouseReleaseEvent(QMouseEvent* _e);

    ///
    void paintEvent(QPaintEvent* _e);


private:
    ///
    QString svgNamePressed_;

    ///
    QString svgNameReleased_;

    ///
    QImage rasterBufferPressed_;

    ///
    QImage rasterBufferReleased_;

    ///
    QImage* currentBuffer_;


    ///
    void prepareBuffers_();
    
};

#endif // IMAGE_BUTTON_H
