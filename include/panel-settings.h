#ifndef PANEL_SETTINGS_H
#define PANEL_SETTINGS_H


#include <QWidget>

#include "vehicle-structures.h"

class ImageButton;
class QLabel;
class QLineEdit;


class PanelSettings : public QWidget
{
    Q_OBJECT

public:
    ///
    explicit PanelSettings(QWidget *_parent = Q_NULLPTR);

    ///
    ~PanelSettings();

    ///
    vehicle_init_data_t getVehicleInitData() const;


signals:
    ///
    void signalStartButton();

    ///
    void signalStopButton();

    ///
    void signalInitDataChanged();

    ///
    void signalStepPressed(int _idx);

    ///
    void signalStepReleased(int _idx);


public slots:
    ///
    void slotProcessStarted();

    ///
    void slotProcessPaused();

    ///
    void slotProcessStopped();

    ///
    void slotProcessFinished();


private slots:
    ///
    void resizeEvent(QResizeEvent* _e);

    ///
    bool eventFilter(QObject* _w, QEvent* _e);

    ///
    void slotButtonOkClicked();


private:
    ///
    QLabel* labX_;

    ///
    QLabel* labV_;

    ///
    QLabel* labA_;

    ///
    QLineEdit* editX_;

    ///
    QLineEdit* editV_;

    ///
    QLineEdit* editA_;

    ///
    QLabel* labBrX_;

    ///
    QLabel* labBrLen_;

    ///
    QLineEdit* editBrX_;

    ///
    QLineEdit* editBrLen_;

    ///
    QLineEdit* editStep1_;

    ///
    QLineEdit* editStep2_;

    ///
    QLineEdit* editStep3_;

    ///
    ImageButton* step1_;

    ///
    ImageButton* step2_;

    ///
    ImageButton* step3_;

    ///
    ImageButton* buttonStart_;

    ///
    ImageButton* buttonStop_;

    ///
    ImageButton* buttonOk_;

};

#endif // PANEL_SETTINGS_H
