#ifndef HUMP_TYPEDEF_H
#define HUMP_TYPEDEF_H

#include <QPoint>
#include <QSize>
#include <QRect>

typedef int h_ln;

typedef float h_flt;

typedef QPoint h_pt;

typedef QSize h_sz;

typedef QRect h_rct;


template <typename T>
h_ln hLn(T _val)
{
    return static_cast<h_ln>(_val);
}


template <typename T>
h_flt hFlt(T _val)
{
    return static_cast<h_flt>(_val);
}

#endif // HUMP_TYPEDEF_H
