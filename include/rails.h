#ifndef RAILS_H
#define RAILS_H

#include "hump-typedef.h"

#include <vector>


class Layout;
class ItemGroup;
class Item;


class Rails
{
public:
    ///
    explicit Rails(Layout* _lay);

    ///
    ~Rails();

    ///
    bool inTupik(h_ln _x) const;

    ///
    h_rct brakeRect() const;

    ///
    void setBrakeAccel(h_flt _step1, h_flt _step2, h_flt _step3);

    ///
    h_flt brakeAccel(int _idx) const;

    ///
    void setBrakeX(h_ln _x);

    ///
    void setBrakeLength(h_ln _len);

private:
    ///
    ItemGroup* view_;

    ///
    Item* rails_;

    ///
    Item* stop_;

    ///
    Item* brake_;

    ///
    std::vector<h_flt> steps_;

};

#endif // RAILS_H
