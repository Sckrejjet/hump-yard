#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H


#include <QMainWindow>


class CentralWidget;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    ///
    explicit MainWindow(QWidget *_parent = Q_NULLPTR);
    ///
    ~MainWindow();


private slots:
    ///
    void keyPressEvent(QKeyEvent* _e);


private:
    ///
    CentralWidget* centralWidget_;

};

#endif // MAIN_WINDOW_H
