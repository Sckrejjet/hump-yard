#ifndef VEHICLE_H
#define VEHICLE_H

#include "hump-typedef.h"

#include <vector>

#include "vehicle-structures.h"

class Layout;
class ItemGroup;
class Item;


class Vehicle
{

public:
    ///
    explicit Vehicle(Layout* lay_);

    ///
    h_ln vehicleEnd() const;

    ///
    void acceptBrake(h_rct _rct, h_flt _btAccel, h_flt _dt);

    ///
    void move(h_ln _x, h_ln _y);

    ///
    void move(h_pt _pos);

    ///
    void step(float _dt);

    ///
    void setVelocity(h_flt _vel);

    ///
    void setAcceleration(h_flt _accel);

    ///
    void setInitData(vehicle_init_data_t _data);

    ///
    void reset();

    ///
    h_flt x() const;

    ///
    h_flt v() const;

    ///
    h_flt a() const;


private:
    ///
    vehicle_init_data_t init_data_;

    ///
    h_flt x_;

    ///
    h_flt v_;

    ///
    h_flt a_;

    ///
    ItemGroup* view_;

    ///
    Item* container_;

    ///
    Item* platform_;

    ///
    Item* wheel1_;

    ///
    Item* wheel2_;

    ///
    Item* wheel3_;

    ///
    Item* wheel4_;

    ///
    std::vector<Item*> vecWheels_;

};

#endif // VEHICLE_H
