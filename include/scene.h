#ifndef SCENE_H
#define SCENE_H

#include "hump-typedef.h"
#include "vehicle-structures.h"


#include <QWidget>

class QTimer;
class QLabel;

class Layout;
class Vehicle;
class Rails;


class Scene : public QWidget
{
    Q_OBJECT

public:
    ///
    explicit Scene(QWidget *_parent = Q_NULLPTR);

    ///
    ~Scene();

    ///
    void addLayout(Layout* _lay);

    ///
    void setVehicleInitData(vehicle_init_data_t _data);


signals:
    ///
    void signalProcessStarted();

    ///
    void signalProcessPaused();

    ///
    void signalProcessStopped();

    ///
    void signalProcessFinished();


public slots:
    ///
    void slotStartResumeProcess();

    ///
    void slotStopProcess();

    ///
    void slotStepPressed(int _idx);

    ///
    void slotStepReleased(int _idx);


private slots:
    ///
    void paintEvent(QPaintEvent* _e);

    ///
    void resizeEvent(QResizeEvent* _e);

    ///
    void slotTimerProcess_();


private:
    ///
    Layout* lay_;

    ///
    h_flt t_;

    ///
    h_flt dt_;

    ///
    int brIdx_;

    ///
    std::vector<Layout*> vecLayouts_;

    ///
    Vehicle* vehicle_;

    ///
    Rails* rails_;

    ///
    QTimer* timerProcess_;

    ///
    QLabel* labX_;

    ///
    QLabel* labV_;

    ///
    QLabel* labA_;


    ///
    void reset_();

};

#endif // SCENE_H
