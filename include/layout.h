#ifndef LAYOUT_H
#define LAYOUT_H

#include "hump-typedef.h"

#include <QSize>

#include <vector>
#include <functional>

class QPainter;

class Scene;
class ItemGroup;


class Layout
{
public:
    ///
    explicit Layout(Scene* _scene);

    ///
    virtual ~Layout();

    ///
    enum ScaleType
    {
        ST_ALL,
        ST_X,
        ST_Y
    };

    ///
    void setScaleType(ScaleType _st);

    ///
    void move(h_pt _pos);

    ///
    void move(h_ln _x, h_ln _y);

    ///
    h_pt pos() const;

    ///
    void setDefaultSize(h_sz _defSz);

    ///
    void setDefaultSize(h_ln _w, h_ln _h);

    ///
    h_sz defaultSize() const;

    ///
    void resize(h_sz _newSz);

    ///
    void resize(h_ln _w, h_ln _h);

    ///
    h_sz size() const;

    ///
    h_ln fX(h_ln _x);

    ///
    h_ln fY(h_ln _y);

    ///
    h_pt fPt(h_pt _pt);

    ///
    h_sz fSz(h_sz _sz);

    ///
    virtual void draw(QPainter& _p);

    ///
    void updateScene();

    ///
    void addItemGroup(ItemGroup* _itGr);


protected:
    ///
    Scene* parentScene_;

    ///
    h_pt pos_;

    ///
    h_sz defaultSize_;

    ///
    h_sz newSize_;

    ///
    ScaleType scaleType_;

    ///
    h_flt factorX_;

    ///
    h_flt factorY_;

    ///
    std::function<h_sz(h_sz)> fcXFun_;

    ///
    std::function<h_sz(h_sz)> fcYFun_;


private:
    ///
    void calcFactor_();

    ///
    void applyFactor_();

    ///
    std::vector<ItemGroup*> vecItemGroups_;

};

#endif // LAYOUT_H
