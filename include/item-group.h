#ifndef ITEMGROUP_H
#define ITEMGROUP_H

#include "hump-typedef.h"

#include <vector>

class QPainter;

class Layout;
class Item;


class ItemGroup
{
public:
    ///
    ItemGroup(Layout* _lay);

    ///
    Layout* lay() const;

    ///
    void move(h_ln _x, h_ln _y);

    ///
    void move(h_pt _pt);

    ///
    h_pt pos() const;

    ///
    void applyFactor();

    ///
    void addItem(Item* _it);

    ///
    void draw(QPainter& _p);

private:
    ///
    Layout* lay_;

    ///
    h_pt pos_;

    ///
    std::vector<Item*> vecItems_;

};

#endif // ITEMGROUP_H
