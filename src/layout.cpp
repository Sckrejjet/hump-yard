#include "layout.h"

#include <QPainter>

#include "scene.h"
#include "item-group.h"


Layout::Layout(Scene *_scene)
    : parentScene_(_scene)
    , scaleType_(ST_ALL)
{
    parentScene_->addLayout(this);
}

Layout::~Layout()
{

}

void Layout::setScaleType(Layout::ScaleType _st)
{
    scaleType_ = _st;
}

void Layout::move(h_pt _pos)
{
    this->pos_ = _pos;
}

void Layout::move(h_ln _x, h_ln _y)
{
    this->pos_.setX(_x);
    this->pos_.setY(_y);
}

h_pt Layout::pos() const
{
    return this->pos_;
}

void Layout::setDefaultSize(h_sz _defSz)
{
    this->setDefaultSize(_defSz.width(), _defSz.height());
}

void Layout::setDefaultSize(h_ln _w, h_ln _h)
{
    this->defaultSize_.setWidth(_w);
    this->defaultSize_.setHeight(_h);
    calcFactor_();
}

h_sz Layout::defaultSize() const
{
    return  this->defaultSize_;
}

void Layout::resize(h_sz _newSz)
{
    this->resize(_newSz.width(), _newSz.height());
}

void Layout::resize(h_ln _w, h_ln _h)
{
    this->newSize_.setWidth(_w);
    this->newSize_.setHeight(_h);
    calcFactor_();
}

h_sz Layout::size() const
{
    return  newSize_;
}

h_ln Layout::fX(h_ln _x)
{
    return  hLn(hFlt(_x)*factorX_);
}

h_ln Layout::fY(h_ln _y)
{
    return  hLn(hFlt(_y)*factorY_);
}

h_pt Layout::fPt(h_pt _pt)
{
    return  h_pt(fX(_pt.x()), fY(_pt.y()));
}

h_sz Layout::fSz(h_sz _sz)
{
    return  h_sz(fX(_sz.width()), fY(_sz.height()));
}

void Layout::draw(QPainter &_p)
{
//    _p.setPen(Qt::black);
//    _p.setBrush(Qt::transparent);
//    _p.drawRect(QRect(pos_, newSize_));

    for (auto itGr : vecItemGroups_)
    {
        itGr->draw(_p);
    }
}

void Layout::updateScene()
{
    parentScene_->update();
}

void Layout::addItemGroup(ItemGroup *_itGr)
{
    vecItemGroups_.push_back(_itGr);
}

void Layout::calcFactor_()
{
    switch (scaleType_) {
    case ST_ALL:
        factorX_ = hFlt(newSize_.width()) / hFlt(defaultSize_.width());
        factorY_ = hFlt(newSize_.height()) / hFlt(defaultSize_.height());
        break;
    case ST_X:
        factorX_ = hFlt(newSize_.width()) / hFlt(defaultSize_.width());
        factorY_ = factorX_;
        break;
    case ST_Y:
        factorY_ = hFlt(newSize_.height()) / hFlt(defaultSize_.height());
        factorX_ = factorY_;
        break;
    }

    applyFactor_();
}

void Layout::applyFactor_()
{
    for (auto itGr : vecItemGroups_)
    {
        itGr->applyFactor();
    }
}
