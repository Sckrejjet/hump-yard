#include "panel-settings.h"

#include <QLabel>
#include <QLineEdit>
#include <QEvent>

#include "image-button.h"


namespace
{
    const QFont FNT("sans-serif", 14);

    void prepareLabel(QLabel* _lab)
    {
        _lab->setFont(FNT);
        _lab->resize(55, 24);
        _lab->setStyleSheet("color: blue;");
    }

    void prepareEdit(QLineEdit* _ed)
    {
        _ed->setFont(FNT);
        _ed->resize(50, 24);
        _ed->setStyleSheet("color: blue;");
    }
}


PanelSettings::PanelSettings(QWidget *_parent)
    : QWidget(_parent)
{
    step1_ = new ImageButton(this);
    step1_->resize(60, 60);
    step1_->setSvg("../resources/images/svg/first-p.svg",
                   "../resources/images/svg/first-r.svg");
    connect(step1_, &ImageButton::signalPressed, [&]()
    {
        emit this->signalStepPressed(1);
    });
    connect(step1_, &ImageButton::signalReleased, [&]()
    {
        emit this->signalStepReleased(1);
    });

    step2_ = new ImageButton(this);
    step2_->resize(60, 60);
    step2_->setSvg("../resources/images/svg/second-p.svg",
                   "../resources/images/svg/second-r.svg");
    connect(step2_, &ImageButton::signalPressed, [&]()
    {
        emit this->signalStepPressed(2);
    });
    connect(step2_, &ImageButton::signalReleased, [&]()
    {
        emit this->signalStepReleased(2);
    });

    step3_ = new ImageButton(this);
    step3_->resize(60, 60);
    step3_->setSvg("../resources/images/svg/third-p.svg",
                   "../resources/images/svg/third-r.svg");
    connect(step3_, &ImageButton::signalPressed, [&]()
    {
        emit this->signalStepPressed(3);
    });
    connect(step3_, &ImageButton::signalReleased, [&]()
    {
        emit this->signalStepReleased(3);
    });

    editStep1_ = new QLineEdit("-5", this);
    prepareEdit(editStep1_);

    editStep2_ = new QLineEdit("-10", this);
    prepareEdit(editStep2_);

    editStep3_ = new QLineEdit("-20", this);
    prepareEdit(editStep3_);

    buttonStart_ = new ImageButton(this);
    buttonStart_->setSvg("../resources/images/svg/start-p.svg",
                         "../resources/images/svg/start-r.svg");
    buttonStart_->resize(80, 80);
    connect(buttonStart_, &QPushButton::clicked,
            this, &PanelSettings::signalStartButton);

    buttonStop_ = new ImageButton(this);
    buttonStop_->setSvg("../resources/images/svg/stop-p.svg",
                        "../resources/images/svg/stop-r.svg");
    buttonStop_->resize(80, 80);
    buttonStop_->setVisible(false);
    connect(buttonStop_, &QPushButton::clicked,
            this, &PanelSettings::signalStopButton);

    buttonOk_ = new ImageButton(this);
    buttonOk_->setSvg("../resources/images/svg/ok-p.svg",
                        "../resources/images/svg/ok-r.svg");
    buttonOk_->resize(80, 80);
    buttonOk_->setVisible(false);
    connect(buttonOk_, &QPushButton::clicked,
            this, &PanelSettings::slotButtonOkClicked);



    labX_ = new QLabel("init X:", this);
    prepareLabel(labX_);

    labV_ = new QLabel("init V:", this);
    prepareLabel(labV_);

    labA_ = new QLabel("init a:", this);
    prepareLabel(labA_);

    editX_ = new QLineEdit("10", this);
    prepareEdit(editX_);

    editV_ = new QLineEdit("10", this);
    prepareEdit(editV_);

    editA_ = new QLineEdit("0", this);
    prepareEdit(editA_);

    labBrX_ = new QLabel("brk X:", this);
    prepareLabel(labBrX_);

    labBrLen_ = new QLabel("brk L:", this);
    prepareLabel(labBrLen_);

    editBrX_ = new QLineEdit("60", this);
    prepareEdit(editBrX_);

    editBrLen_ = new QLineEdit("28", this);
    prepareEdit(editBrLen_);

    editX_->installEventFilter(this);
    editV_->installEventFilter(this);
    editA_->installEventFilter(this);
    editBrX_->installEventFilter(this);
    editBrLen_->installEventFilter(this);
    editStep1_->installEventFilter(this);
    editStep2_->installEventFilter(this);
    editStep3_->installEventFilter(this);
}




PanelSettings::~PanelSettings()
{

}




vehicle_init_data_t PanelSettings::getVehicleInitData() const
{
    vehicle_init_data_t data;
    data.x = toMPS(editX_->text().toFloat());
    data.v = toMPS(editV_->text().toFloat());
    data.a = toMPS(editA_->text().toFloat());

    data.brX = toMPS(editBrX_->text().toFloat());
    data.brLen = toMPS(editBrLen_->text().toFloat());

    data.step1 = toMPS(editStep1_->text().toFloat());
    data.step2 = toMPS(editStep2_->text().toFloat());
    data.step3 = toMPS(editStep3_->text().toFloat());

    return data;
}




void PanelSettings::slotProcessStarted()
{
    buttonStart_->setSvg("../resources/images/svg/pause-p.svg",
                         "../resources/images/svg/pause-r.svg");

    buttonStop_->show();

    editX_->setEnabled(false);
    editV_->setEnabled(false);
    editA_->setEnabled(false);
    editBrX_->setEnabled(false);
    editBrLen_->setEnabled(false);
    editStep1_->setEnabled(false);
    editStep2_->setEnabled(false);
    editStep3_->setEnabled(false);
}




void PanelSettings::slotProcessPaused()
{
    buttonStart_->setSvg("../resources/images/svg/start-p.svg",
                         "../resources/images/svg/start-r.svg");
}




void PanelSettings::slotProcessStopped()
{
    buttonStart_->setSvg("../resources/images/svg/start-p.svg",
                         "../resources/images/svg/start-r.svg");

    buttonStop_->hide();
    buttonStart_->show();

    editX_->setEnabled(true);
    editV_->setEnabled(true);
    editA_->setEnabled(true);
    editBrX_->setEnabled(true);
    editBrLen_->setEnabled(true);
    editStep1_->setEnabled(true);
    editStep2_->setEnabled(true);
    editStep3_->setEnabled(true);
}




void PanelSettings::slotProcessFinished()
{
    buttonStart_->setSvg("../resources/images/svg/start-p.svg",
                         "../resources/images/svg/start-r.svg");

    buttonStart_->hide();
}




void PanelSettings::resizeEvent(QResizeEvent* _e)
{
    step1_->move(350, 5);
    step2_->move(500, 5);
    step3_->move(650, 5);

    editStep1_->move(step1_->x() - (editStep1_->width() - step1_->width())/2,
            step1_->y() + step1_->height() + 5);
    editStep2_->move(step2_->x() - (editStep2_->width() - step2_->width())/2,
                     step2_->y() + step2_->height() + 5);
    editStep3_->move(step3_->x() - (editStep3_->width() - step3_->width())/2,
                     step3_->y() + step3_->height() + 5);

    buttonStop_->move(this->width() - buttonStop_->width() - 20,
                      (this->height() - buttonStop_->height())/2);
    buttonStart_->move(this->width() - buttonStop_->width() - buttonStart_->width() - 40,
                       (this->height() - buttonStart_->height())/2);
    buttonOk_->move(buttonStop_->pos());

    labX_->move(10, 5);
    labV_->move(10, labX_->y() + labX_->height() + 10);
    labA_->move(10, labV_->y() + labV_->height() + 10);

    editX_->move(labX_->x() + labX_->width() + 10, 5);
    editV_->move(labX_->x() + labX_->width() + 10, editX_->y() + editX_->height() + 10);
    editA_->move(labX_->x() + labX_->width() + 10, editV_->y() + editV_->height() + 10);

    labBrX_->move(180, 5);
    labBrLen_->move(180, labBrX_->y() + labBrX_->height() + 10);

    editBrX_->move(labBrX_->x() + labBrX_->width() + 10, labBrX_->y());
    editBrLen_->move(labBrLen_->x() + labBrLen_->width() + 10, labBrLen_->y());
}

bool PanelSettings::eventFilter(QObject* _w, QEvent* _e)
{
    if (_w == editX_ || _w == editV_ || _w == editA_ || _w == editBrLen_ || _w == editBrX_ || _w == editStep1_ || _w == editStep2_ || _w == editStep3_)
    {
        if (_e->type() == QEvent::KeyPress)
        {
            buttonStart_->hide();
            buttonOk_->show();
        }
    }

    return  QWidget::eventFilter(_w, _e);
}

void PanelSettings::slotButtonOkClicked()
{
    buttonOk_->hide();
    buttonStart_->show();

    emit signalInitDataChanged();
}
