#include "central-widget.h"


#include <QResizeEvent>

#include "scene.h"
#include "panel-settings.h"


CentralWidget::CentralWidget(QWidget *_parent)
    : QWidget(_parent)
{
    scene_ = new Scene(this);

    panelSettings_ = new PanelSettings(this);

    connect(panelSettings_, &PanelSettings::signalStartButton,
            scene_, &Scene::slotStartResumeProcess);

    connect(panelSettings_, &PanelSettings::signalStopButton,
            scene_, &Scene::slotStopProcess);

    connect(scene_, &Scene::signalProcessStarted,
            panelSettings_, &PanelSettings::slotProcessStarted);

    connect(scene_, &Scene::signalProcessPaused,
            panelSettings_, &PanelSettings::slotProcessPaused);

    connect(scene_, &Scene::signalProcessStopped,
            panelSettings_, &PanelSettings::slotProcessStopped);

    connect(scene_, &Scene::signalProcessFinished,
            panelSettings_, &PanelSettings::slotProcessFinished);

    connect(panelSettings_, &PanelSettings::signalInitDataChanged,
            this, &CentralWidget::slotInitDataChanged);

    connect(panelSettings_, &PanelSettings::signalStepPressed,
            scene_, &Scene::slotStepPressed);

    connect(panelSettings_, &PanelSettings::signalStepReleased,
            scene_, &Scene::slotStepReleased);

    scene_->setVehicleInitData(panelSettings_->getVehicleInitData());
}

CentralWidget::~CentralWidget()
{

}

void CentralWidget::slotInitDataChanged()
{
    scene_->setVehicleInitData(panelSettings_->getVehicleInitData());
}

void CentralWidget::resizeEvent(QResizeEvent *_e)
{
    panelSettings_->resize(this->width(), 100);
    panelSettings_->move(0, this->height() - panelSettings_->height());

    scene_->resize(this->width(), this->height() - panelSettings_->height());
    scene_->move(0, 0);
}
