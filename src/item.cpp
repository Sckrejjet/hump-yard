#include "item.h"

#include <QSvgRenderer>
#include <QPainter>

#include "layout.h"
#include "item-group.h"

Item::Item(Layout *_parentLay)
    : lay_(_parentLay)
    , gr_(new ItemGroup(_parentLay))
{
    gr_->addItem(this);
}

Item::Item(ItemGroup *_group)
    : lay_(_group->lay())
    , gr_(_group)
{
    gr_->addItem(this);
}

void Item::setSvg(QString _svgName)
{
    svgName_ = _svgName;

    prepareBuffer_();
}

void Item::move(h_ln _x, h_ln _y)
{
    this->pos_.setX(_x);
    this->pos_.setY(_y);
}

void Item::move(h_pt _pt)
{
    move(_pt.x(), _pt.y());
}

h_pt Item::pos() const
{
    return pos_;
}

//void Item::setDefaultSize(h_ln _w, h_ln _h)
//{
//    this->defaultSize_.setWidth(_w);
//    this->defaultSize_.setHeight(_h);
//}

//void Item::setDefaultSize(h_sz _sz)
//{
//    setDefaultSize(_sz.width(), _sz.height());
//}

void Item::resize(h_ln _w, h_ln _h)
{
    this->size_.setWidth(_w);
    this->size_.setHeight(_h);

    prepareBuffer_();
}

void Item::resize(h_sz _sz)
{
    this->resize(_sz.width(), _sz.height());
}

h_sz Item::size() const
{
    return size_;
}

void Item::draw(QPainter &_p)
{
    _p.drawImage(lay_->pos() + lay_->fPt(gr_->pos() + pos_), rasterBuffer_);
}

void Item::applyFactor()
{
    prepareBuffer_();
}

void Item::prepareBuffer_()
{
    rasterBuffer_ = QImage(lay_->fSz(this->size_), QImage::Format_ARGB32_Premultiplied);
    rasterBuffer_.fill(Qt::transparent);

    QPainter p(&rasterBuffer_);

    QSvgRenderer r(svgName_);
    r.render(&p);

    p.end();
}
