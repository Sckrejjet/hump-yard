#include "item-group.h"

#include "QPainter"

#include "layout.h"
#include "item.h"

ItemGroup::ItemGroup(Layout *_lay)
    : lay_(_lay)
{
    lay_->addItemGroup(this);
}

Layout *ItemGroup::lay() const
{
    return lay_;
}

void ItemGroup::move(h_ln _x, h_ln _y)
{
    pos_.setX(_x);
    pos_.setY(_y);

    lay_->updateScene();
}

void ItemGroup::move(h_pt _pt)
{
    move(_pt.x(), _pt.y());
}

h_pt ItemGroup::pos() const
{
    return this->pos_;
}

void ItemGroup::applyFactor()
{
    for (auto it : vecItems_)
    {
        it->applyFactor();
    }

    lay_->updateScene();
}

void ItemGroup::addItem(Item *_it)
{
    vecItems_.push_back(_it);

    lay_->updateScene();
}

void ItemGroup::draw(QPainter &_p)
{
    for (auto it : vecItems_)
    {
        it->draw(_p);
    }
}
