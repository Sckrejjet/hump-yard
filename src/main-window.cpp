#include "main-window.h"

#include <QKeyEvent>

#include "central-widget.h"


MainWindow::MainWindow(QWidget *_parent)
    : QMainWindow(_parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowState(Qt::WindowFullScreen);

    centralWidget_ = new CentralWidget();

    this->setCentralWidget(centralWidget_);
}



MainWindow::~MainWindow()
{

}




void MainWindow::keyPressEvent(QKeyEvent *_e)
{
    switch (_e->key()) {
    case Qt::Key_Escape:
        this->close();
        break;
//    case Qt::Key_S:
//        centralWidget_->startProcess();
//        break;
//    case Qt::Key_Up:
//        lay_->resize(lay_->size() + QSize(16, 9));
//        break;
//    case Qt::Key_Down:
//        lay_->resize(lay_->size() - QSize(16, 9));
//        break;
//    case Qt::Key_Right:
//        vehicle_->move(vehicle_->pos() + h_pt(10, 0));
//        break;
//    case Qt::Key_Left:
//        vehicle_->move(vehicle_->pos() - h_pt(10, 0));
//        break;
    default:
        break;
    }
    centralWidget_->update();
}
