#include "image-button.h"

#include <QPainter>
#include <QSvgRenderer>
#include <QMouseEvent>

ImageButton::ImageButton(QWidget* _parent)
    : QPushButton(_parent)
{
    prepareBuffers_();
}

ImageButton::~ImageButton()
{

}

void ImageButton::setSvg(QString _pressed, QString _released)
{
    svgNamePressed_ = _pressed;
    svgNameReleased_ = _released;
    prepareBuffers_();
    this->update();
}

void ImageButton::resizeEvent(QResizeEvent* _e)
{
    prepareBuffers_();
}

void ImageButton::mousePressEvent(QMouseEvent* _e)
{
    QPushButton::mousePressEvent(_e);

    if (_e->button() == Qt::LeftButton)
    {
        currentBuffer_ = &rasterBufferPressed_;
        this->update();
    }

    emit signalPressed();
}

void ImageButton::mouseReleaseEvent(QMouseEvent* _e)
{
    QPushButton::mouseReleaseEvent(_e);

    if (_e->button() == Qt::LeftButton)
    {
        currentBuffer_ = &rasterBufferReleased_;
        this->update();
    }

    emit signalReleased();
}

//void ImageButton::keyPressEvent(QKeyEvent* _e)
//{
//    currentBuffer_ = &rasterBufferPressed_;
//    this->update();
//}

//void ImageButton::keyReleaseEvent(QKeyEvent* _e)
//{
//    currentBuffer_ = &rasterBufferReleased_;
//    this->update();
//}

void ImageButton::paintEvent(QPaintEvent* _e)
{
    QPainter p(this);
    p.drawImage(0, 0, *currentBuffer_);
    p.end();
}

void ImageButton::prepareBuffers_()
{
    QPainter p;
    QSvgRenderer r;

    rasterBufferPressed_ = QImage(this->size(), QImage::Format_ARGB32_Premultiplied);
    rasterBufferPressed_.fill(Qt::transparent);

    p.begin(&rasterBufferPressed_);
    r.load(svgNamePressed_);
    r.render(&p);
    p.end();

    rasterBufferReleased_ = QImage(this->size(), QImage::Format_ARGB32_Premultiplied);
    rasterBufferReleased_.fill(Qt::transparent);

    p.begin(&rasterBufferReleased_);
    r.load(svgNameReleased_);
    r.render(&p);
    p.end();

    currentBuffer_ = &rasterBufferReleased_;
}
