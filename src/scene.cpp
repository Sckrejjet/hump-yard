#include "scene.h"

#include <QTimer>
#include <QPainter>
#include <QLabel>

#include "layout.h"
#include "vehicle.h"
#include "rails.h"
#include "item-group.h"


Scene::Scene(QWidget *_parent)
    : QWidget(_parent)
    , t_(0.0f)
    , dt_(0.01f)
    , brIdx_(0)
{
    lay_ = new Layout(this);
    lay_->setDefaultSize(1920*2, 1080*2);
    lay_->resize(1920, 1080);
    lay_->move(0, 0);

    timerProcess_ = new QTimer(this);
    timerProcess_->setInterval(hLn(dt_*1000));

    connect(timerProcess_, &QTimer::timeout,
            this, &Scene::slotTimerProcess_);

    vehicle_ = new Vehicle(lay_);
//    vehicle_->setVelocity(toMPS(14));
//    vehicle_->setAcceleration(toMPS(0.0f));
    vehicle_->move(10, 1060);

//    vehicle_init_data_t veh_data;
//    veh_data.x = 10;
//    veh_data.v = toMPS(14.0f);
//    veh_data.a = toMPS(0.0f);

//    vehicle_->setInitData(veh_data);
//    vehicle_->reset();

    rails_ = new Rails(lay_);
//    rails_->setBrakeAccel(toMPS(-250));

    labX_ = new QLabel("0", this);
    labX_->resize(100, 20);
    labX_->move(10, 5);
    labV_ = new QLabel("0", this);
    labV_->resize(100, 20);
    labV_->move(10, 25);
    labA_ = new QLabel("0", this);
    labA_->resize(100, 20);
    labA_->move(10, 45);
}




Scene::~Scene()
{

}




void Scene::addLayout(Layout *_lay)
{
    vecLayouts_.push_back(_lay);
}




void Scene::setVehicleInitData(vehicle_init_data_t _data)
{
    vehicle_->setInitData(_data);
    vehicle_->reset();

    rails_->setBrakeX(_data.brX);
    rails_->setBrakeLength(_data.brLen);

    rails_->setBrakeAccel(_data.step1, _data.step2, _data.step3);
}




void Scene::slotStartResumeProcess()
{
    if (timerProcess_->isActive())
    {
        timerProcess_->stop();
        emit signalProcessPaused();
    }
    else
    {
        timerProcess_->start();
        emit signalProcessStarted();
    }
}

void Scene::slotStopProcess()
{
    timerProcess_->stop();

    emit signalProcessStopped();

    reset_();
}

void Scene::slotStepPressed(int _idx)
{
    brIdx_ = _idx;
}

void Scene::slotStepReleased(int _idx)
{
    Q_UNUSED(_idx);

    brIdx_ = 0;
}




void Scene::paintEvent(QPaintEvent *_e)
{
    Q_UNUSED(_e);

    QPainter p;
    p.begin(this);

    p.setPen(Qt::red);
    p.setBrush(QColor(255, 255, 220));
    p.drawRect(this->rect());

    for (auto lay : vecLayouts_)
    {
        lay->draw(p);
    }

    p.end();
}




void Scene::resizeEvent(QResizeEvent *_e)
{
    lay_->resize(this->size());
}




void Scene::slotTimerProcess_()
{
    if (rails_->inTupik(vehicle_->vehicleEnd()))
    {
        timerProcess_->stop();
        emit signalProcessFinished();
        return;
    }

    t_ += dt_;

    vehicle_->acceptBrake(rails_->brakeRect(), toMPS(rails_->brakeAccel(brIdx_)), dt_);
    vehicle_->step(dt_);

    labX_->setText(QString::number(fromMPS(vehicle_->x())));
    labV_->setText(QString::number(fromMPS(vehicle_->v())));
    labA_->setText(QString::number(fromMPS(vehicle_->a())));

    this->update();
}

void Scene::reset_()
{
    vehicle_->reset();
}
