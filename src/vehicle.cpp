#include "vehicle.h"

#include "layout.h"
#include "item-group.h"
#include "item.h"

Vehicle::Vehicle(Layout *lay_)
{
    view_ = new ItemGroup(lay_);

    container_ = new Item(view_);
    container_->setSvg("../resources/images/svg/container.svg");
    container_->resize(300, 100);

    wheel1_ = new Item(view_);
    wheel1_->setSvg("../resources/images/svg/wheel.svg");
    wheel1_->resize(40, 40);
    wheel1_->move(5, 105);

    wheel2_ = new Item(view_);
    wheel2_->setSvg("../resources/images/svg/wheel.svg");
    wheel2_->resize(40, 40);
    wheel2_->move(50, 105);

    wheel3_ = new Item(view_);
    wheel3_->setSvg("../resources/images/svg/wheel.svg");
    wheel3_->resize(40, 40);
    wheel3_->move(210, 105);

    wheel4_ = new Item(view_);
    wheel4_->setSvg("../resources/images/svg/wheel.svg");
    wheel4_->resize(40, 40);
    wheel4_->move(255, 105);

    vecWheels_.push_back(wheel1_);
    vecWheels_.push_back(wheel2_);
    vecWheels_.push_back(wheel3_);
    vecWheels_.push_back(wheel4_);

    platform_ = new Item(view_);
    platform_->setSvg("../resources/images/svg/platform.svg");
    platform_->resize(300, 30);
    platform_->move(0, 95);
}

h_ln Vehicle::vehicleEnd() const
{
    return view_->pos().x() + container_->size().width();
}

void Vehicle::acceptBrake(h_rct _rct, h_flt _btAccel, h_flt _dt)
{
    a_ = 0.0f;

    for (auto wh : vecWheels_)
    {
        if (this->x_ + wh->pos().x() + wh->size().width() - 5 > _rct.x() && this->x_ + wh->pos().x() + 5 < _rct.x() + _rct.width())
        {
            a_ += _btAccel/4.0f*_dt;
        }
    }
}

void Vehicle::move(h_ln _x, h_ln _y)
{
    x_ = _x;
    view_->move(_x, _y);
}

void Vehicle::move(h_pt _pos)
{
    this->move(_pos.x(), _pos.y());
}

void Vehicle::step(float _dt)
{
    v_ += a_*_dt;
    if (v_ < 0.0f)
        v_ = 0.0f;
    x_ += v_*_dt;

    view_->move(hLn(x_), view_->pos().y());
}

void Vehicle::setVelocity(h_flt _vel)
{
    v_ = _vel;
}

void Vehicle::setAcceleration(h_flt _accel)
{
    a_ = _accel;
}

void Vehicle::setInitData(vehicle_init_data_t _data)
{
    init_data_ = _data;
}

void Vehicle::reset()
{
    x_ = init_data_.x;
    v_ = init_data_.v;
    a_ = init_data_.a;

    view_->move(x_, view_->pos().y());
}

h_flt Vehicle::x() const
{
    return x_;
}

h_flt Vehicle::v() const
{
    return v_;
}

h_flt Vehicle::a() const
{
    return  a_;
}
