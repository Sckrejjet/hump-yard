#include "rails.h"


#include "item-group.h"
#include "item.h"


Rails::Rails(Layout* _lay)
{
    view_ = new ItemGroup(_lay);
    view_->move(0, 1200);

    rails_ = new Item(view_);
    rails_->setSvg("../resources/images/svg/rail.svg");
    rails_->resize(1880*2, 10);
    rails_->move(10, 0);

    stop_ = new Item(view_);
    stop_->setSvg("../resources/images/svg/tupik.svg");
    stop_->resize(100, 100);
    stop_->move(1880*2, -90);

    brake_ = new Item(view_);
    brake_->setSvg("../resources/images/svg/brake.svg");
    brake_->resize(600, 14);
    brake_->move(1300, -4);

    steps_.resize(4);
    steps_[0] = 0.0f;
}

Rails::~Rails()
{

}

bool Rails::inTupik(h_ln _x) const
{
    return _x >= stop_->pos().x();
}

h_rct Rails::brakeRect() const
{
    return  {brake_->pos(), brake_->size()};
}

void Rails::setBrakeAccel(h_flt _step1, h_flt _step2, h_flt _step3)
{
    steps_[1] = _step1;
    steps_[2] = _step2;
    steps_[3] = _step3;
}

h_flt Rails::brakeAccel(int _idx) const
{
    return steps_[_idx];
}

void Rails::setBrakeX(h_ln _x)
{
    brake_->move(_x, brake_->pos().y());
}

void Rails::setBrakeLength(h_ln _len)
{
    brake_->resize(_len, brake_->size().height());
}
