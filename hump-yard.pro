#-------------------------------------------------
#
# Project created by QtCreator 2018-11-06T21:36:43
#
#-------------------------------------------------

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++14

QT       += core gui svg
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
DESTDIR = ../bin

CONFIG(debug, debug|release){

    # Debug mode
    TARGET = hump-yard_d

}else{

    # Release mode
    TARGET = hump-yard

}


INCLUDEPATH += ./include

HEADERS += $$files(./include/*.h)

SOURCES += $$files(./src/*.cpp)
